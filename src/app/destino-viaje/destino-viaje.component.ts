// Indicamos todos los artefactos que vamos a usar
import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';  
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.scss']
})
export class DestinoViajeComponent implements OnInit {
  /* destino es suceptible de ser pasado como parametro en el tag
     app-destino-viaje */
  @Input() destino: DestinoViaje;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  /* Declaramos un nuevo emitidor de eventos para DestinoViaje, por lo que
     cuando lo emitimos le debemos pasar un destino viaje.
    <DestinoViaje> indica el tipo de dato del generic EventEmmiter */
  @Output() clicked: EventEmitter<DestinoViaje>; 

  constructor() {
    this.clicked = new EventEmitter();  // Inicializamos el emitidor
  }

  ngOnInit(): void {}

  ir() {
    /* 
      Acción de desencadenamiento del evento.
      Clciked es un atributo de nuestro objeto que nos permite disparar
      eventos. Damos a conocer al componente padre que destino fue clickeado.
    */
    this.clicked.emit(this.destino);
    return false;
  }
}
