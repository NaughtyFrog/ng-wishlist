export class DestinoViaje {
  private selected: boolean;
  public servicios: string[];

  constructor(public nombre: string, public imagenUrl: string) {
    this.servicios = ['pileta', 'desayuno'];
  }
  /* Equivalente a:
      nombre: string;
      imagenUrl: string;
      constructor(n: string, u: string) {
        this.nombre = n;
        this.imagenUrl = u;
      }
  */

  isSelected(): boolean {
    return this.selected;
  }

  setSelected(s: boolean) {
    this.selected = s;
  }
}
